import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { Data } from './data.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  loadedData: Data[] = [];
  singleData: any;
  allDataClicked = false;
  isFetching = false;

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void { }


  onFetchData() {
    this.fetchData();
  }

  onFetchSingleData() {
    this.fetchSingleData();
  }

  private fetchData() {
    this.isFetching = true;
    this.allDataClicked = true;
    this.http
      .get<Data[]>('https://jsonplaceholder.typicode.com/todos')
      .pipe(
        map(responseData => {
          const dataArray: Data[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              dataArray.push({ ...responseData[key], id: key });
            }
          }
          return dataArray;
        })
      )
      .subscribe(data => {
        this.isFetching = false;
        console.log(data);
        this.loadedData = data;
      })
  }

  private fetchSingleData() {
    this.allDataClicked = false;
    this.http
      .get('https://jsonplaceholder.typicode.com/todos/10')
      .subscribe(data => {
        this.isFetching = false;
        console.log(data);
        this.singleData = data;
      })
  }
}
